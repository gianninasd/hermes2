package proc

import (
	"fmt"
	"time"
)

// BillingDetails customer address
type BillingDetails struct {
	Zip string `json:"zip"`
}

// String representation of CardRequest struct
func (b BillingDetails) String() string {
	return fmt.Sprintf("{zip=%v}", b.Zip)
}

// CardExpiry card expiry date
type CardExpiry struct {
	Month string `json:"month"`
	Year  string `json:"year"`
}

// Card card details
type Card struct {
	CardNum    string     `json:"cardNum"`
	CardExpiry CardExpiry `json:"cardExpiry"`
}

// String representation of Card struct
func (c Card) String() string {
	length := len(c.CardNum)
	last4 := c.CardNum[length-4:]
	return fmt.Sprintf("{CardNum=%v, Expiry=00/00}", last4)
}

// CardRequest the full request
type CardRequest struct {
	MerchantRefNum string         `json:"merchantRefNum"`
	Amount         int            `json:"amount"`
	SettleWithAuth bool           `json:"settleWithAuth"`
	Card           Card           `json:"card"`
	BillingDetails BillingDetails `json:"billingDetails"`
}

// String representation of CardRequest struct
func (c CardRequest) String() string {
	return fmt.Sprintf("{MerchantRefNum=%v, Amount=%v, Card=%v, billing=%v}", c.MerchantRefNum, c.Amount, c.Card, c.BillingDetails)
}

// Error for an error response
type Error struct {
	Code    int    `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

// String representation of Error struct
func (e Error) String() string {
	return fmt.Sprintf("{Code=%v, Message=%v}", e.Code, e.Message)
}

// AuthResponse for the response content
type AuthResponse struct {
	ID             string `json:"id"`
	Status         string `json:"status"`
	MerchantRefNum string `json:"merchantRefNum"`
	TxnTime        string `json:"txnTime"`
	SettleWithAuth bool   `json:"settleWithAuth"`
	Amount         int    `json:"amount"`
	AuthCode       string `json:"authCode,omitempty"`
	Error          *Error `json:"error,omitempty"`
}

// String representation of AuthResponse struct
func (r AuthResponse) String() string {
	if r.Status == "FAILED" {
		return fmt.Sprintf("{Status=%v, MerchantRefNum=%v, Error=%v}", r.Status, r.MerchantRefNum, r.Error)
	} else {
		return fmt.Sprintf("{Status=%v, MerchantRefNum=%v, AuthCode=%v}", r.Status, r.MerchantRefNum, r.AuthCode)
	}	
}

// CardResponse contains return code and content
type CardResponse struct {
	HTTPStatusCode int
	RawData        AuthResponse
}

// String representation of CardResponse struct
func (r CardResponse) String() string {
	return fmt.Sprintf("{HTTPStatusCode=%v, RawData=%v}", r.HTTPStatusCode, r.RawData)
}

// Process generate either a successful or error response based on the amount
func Process(uuid string, req CardRequest) CardResponse {
	var response CardResponse
	var rawResponse AuthResponse

	switch req.Amount {
	case 5:
		rawResponse = generateError(uuid, req, 1005, "Transaction declined by the bank")
		response = CardResponse{HTTPStatusCode: 400, RawData: rawResponse}
	case 7:
		rawResponse = generateError(uuid, req, 1007, "Insufficient funds")
		response = CardResponse{HTTPStatusCode: 400, RawData: rawResponse}
	case 17:
		rawResponse = generateError(uuid, req, 4005, "Declined by Risk Management")
		response = CardResponse{HTTPStatusCode: 400, RawData: rawResponse}
	default:
		rawResponse = generateSuccess(uuid, req)
		response = CardResponse{HTTPStatusCode: 200, RawData: rawResponse}
	}

	return response
}

// generates an error response object
func generateError(uuid string, req CardRequest, code int, mess string) AuthResponse {
	currentTime := time.Now()
	cardResponse := AuthResponse{
		ID:             uuid,
		Status:         "FAILED",
		TxnTime:        currentTime.Format("200-01-01 00:00:00"),
		MerchantRefNum: req.MerchantRefNum,
		Amount:         req.Amount,
		SettleWithAuth: req.SettleWithAuth,
		Error:          &Error{Code: code, Message: mess},
	}

	return cardResponse
}

// generates a successful response object
func generateSuccess(uuid string, req CardRequest) AuthResponse {
	currentTime := time.Now()
	cardResponse := AuthResponse{
		ID:             uuid,
		Status:         "COMPLETED",
		TxnTime:        currentTime.Format("200-01-01 00:00:00"),
		MerchantRefNum: req.MerchantRefNum,
		Amount:         req.Amount,
		SettleWithAuth: req.SettleWithAuth,
		AuthCode:       "W08GG1",
	}

	return cardResponse
}
