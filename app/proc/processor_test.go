package proc

import (
	"testing"
)

func TestReturnSuccess(t *testing.T) {
	uuid := "bla-1234"
	req := CardRequest{MerchantRefNum: "test123", Amount: 1500, SettleWithAuth: true}
	var res CardResponse

	if res = Process(uuid, req); res.HTTPStatusCode != 200 {
		t.Error("Expected to be HTTPStatusCode 200", res.HTTPStatusCode)
	}

	if res.String() != "{HTTPStatusCode=200, RawData={Status=COMPLETED, MerchantRefNum=test123, AuthCode=W08GG1}}" {
		t.Error("Expected to be {HTTPStatusCode=200, RawData={Status=COMPLETED, MerchantRefNum=test123, AuthCode=W08GG1}}", res)
	}
}

func TestError7(t *testing.T) {
	uuid := "bla-1234"
	req := CardRequest{MerchantRefNum: "test1234", Amount: 7, SettleWithAuth: true}
	res := Process(uuid, req)

	if res.HTTPStatusCode != 400 {
		t.Error("Expected to be HTTPStatusCode 400", res.HTTPStatusCode)
	}

	if res.RawData.Error.Code != 1007 {
		t.Error("Expected to be error code 1007", res.RawData.Error.Code)
	}

	if res.RawData.Status != "FAILED" {
		t.Error("Expected to be FAILED", res.RawData.Status)
	}

	if res.String() != "{HTTPStatusCode=400, RawData={Status=FAILED, MerchantRefNum=test1234, Error={Code=1007, Message=Insufficient funds}}}" {
		t.Error("Expected to be {HTTPStatusCode=400, RawData={Status=FAILED, MerchantRefNum=test1234,  Error={Code=1007, Message=Insufficient funds}}}", res)
	}
}

func TestError5(t *testing.T) {
	uuid := "bla-1234"
	req := CardRequest{MerchantRefNum: "test1234", Amount: 5, SettleWithAuth: true}
	res := Process(uuid, req)

	if res.HTTPStatusCode != 400 {
		t.Error("Expected to be HTTPStatusCode 400", res.HTTPStatusCode)
	}

	if res.RawData.Error.Code != 1005 {
		t.Error("Expected to be error code 1005", res.RawData.Error.Code)
	}

	if res.RawData.Status != "FAILED" {
		t.Error("Expected to be FAILED", res.RawData.Status)
	}
}
