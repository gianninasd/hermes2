package validation

import (
	proc "dg/cardsim/proc"
)

// ValidationError contains error info
type ValidationError struct {
	Field   string
	Message string
}

// Validate the CardRequest object provided
func Validate(req proc.CardRequest) []ValidationError {
	var result []ValidationError

	if req.Amount == 0 {
		err := ValidationError{Field: "Amount", Message: "Amount must be valid"}
		result = append(result, err)
	}

	if req.MerchantRefNum == "" {
		err := ValidationError{Field: "MerchantRefNum", Message: "MerchantRefNum cannot be empty"}
		result = append(result, err)
	}

	return result
}
