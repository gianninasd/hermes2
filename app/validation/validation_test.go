package validation

import (
	proc "dg/cardsim/proc"
	"testing"
)

func TestNoErrors(t *testing.T) {
	req := proc.CardRequest{MerchantRefNum: "test1234", Amount: 1500, SettleWithAuth: true}

	if errors := Validate(req); len(errors) != 0 {
		t.Error("Expected to have no validation errors", len(errors))
	}
}

func TestBadAmount(t *testing.T) {
	req := proc.CardRequest{MerchantRefNum: "test1234", Amount: 0, SettleWithAuth: true}
	errors := Validate(req)

	if len(errors) == 0 {
		t.Error("Expected to have at least 1 error, got", len(errors))
	}

	if len(errors) > 0 && errors[0].Message != "Amount must be valid" {
		t.Error("Expected Amount must be valid", errors[0].Message)
	}
}

func TestBadRefNum(t *testing.T) {
	req := proc.CardRequest{MerchantRefNum: "", Amount: 1500, SettleWithAuth: true}
	errors := Validate(req)

	if len(errors) == 0 {
		t.Error("Expected to have at least 1 error, got", len(errors))
	}

	if len(errors) > 0 && errors[0].Message != "MerchantRefNum cannot be empty" {
		t.Error("Expected MerchantRefNum cannot be empty", errors[0].Message)
	}
}
