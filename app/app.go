package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"context"

	proc "dg/cardsim/proc"
	val "dg/cardsim/validation"

	guuid "github.com/google/uuid"
	"github.com/gorilla/mux"
	//"github.com/magiconair/properties"
	"github.com/aws/aws-lambda-go/lambda"
)

// processes the root endpoint
func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Card Simulator (GO lang) up and running!")
	log.Println("Endpoint Hit: /")
}

// processes a POST request
func processAuth(resp http.ResponseWriter, req *http.Request) {

	uuid := guuid.New().String()
	log.Println(uuid, "Incoming request from", req.RemoteAddr)

	// get the body of our POST request and return the string response
	reqBody, _ := ioutil.ReadAll(req.Body)

	var cardRequest proc.CardRequest
	json.Unmarshal(reqBody, &cardRequest)
	log.Println(uuid, "Processing", cardRequest)

	// validate request
	var cardResponse proc.CardResponse
	errors := val.Validate(cardRequest)

	if len(errors) > 0 {
		// stop and return if there any validation errors
		authResp := proc.AuthResponse{ID: uuid, Error: &proc.Error{Code: 0, Message: errors[0].Message}}
		cardResponse = proc.CardResponse{HTTPStatusCode: 400, RawData: authResp}
		log.Println(uuid, "Request validation failed:", errors[0].Message)
	} else {
		// run thru simulator
		cardResponse = proc.Process(uuid, cardRequest)
		log.Println(uuid, "Response was", cardResponse)
	}

	resp.Header().Add("Content-Type", "application/json")
	resp.WriteHeader(cardResponse.HTTPStatusCode)
	json.NewEncoder(resp).Encode(cardResponse.RawData)
}

// setups the routing for each endpoint
func setupRouting(port string) {
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/auth", processAuth).Methods("POST")
	log.Fatal(http.ListenAndServe(port, myRouter))
}

// AWS Lambda function processes the incoming request
func HandleRequest(ctx context.Context, cardRequest proc.CardRequest) (proc.CardResponse, error) {
	uuid := guuid.New().String()
	log.Println(uuid, "Received", cardRequest)

	// validate request
	var cardResponse proc.CardResponse
	errors := val.Validate(cardRequest)

	if len(errors) > 0 {
		// stop and return if there any validation errors
		authResp := proc.AuthResponse{ID: uuid, Error: &proc.Error{Code: 0, Message: errors[0].Message}}
		cardResponse = proc.CardResponse{HTTPStatusCode: 400, RawData: authResp}
		log.Println(uuid, "Request validation failed:", errors[0].Message)
	} else {
		// run thru simulator
		cardResponse = proc.Process(uuid, cardRequest)
		log.Println(uuid, "Response was", cardResponse)
	}

	return cardResponse, nil
}

// --------------------------------------------------------
// Where it all starts
// --------------------------------------------------------
func main() {
	log.Println("Go", runtime.Version(), "Card REST Simulator running on", runtime.GOOS, "- Process ID", os.Getpid())

	// load config from the property file
	/*p := properties.MustLoadFile("config.properties", properties.UTF8)

	port := p.MustGetString("port")
	log.Println("Listening on", port)

	setupRouting(port)*/
	lambda.Start(HandleRequest)
}
