module dg/cardsim

go 1.15

require (
	github.com/aws/aws-lambda-go v1.20.0
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	github.com/magiconair/properties v1.8.4
)
