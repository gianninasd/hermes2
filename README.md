# Hermes2
Sample API simulator written in GO lang that deploys as an AWS lambda function

## Pre-requisites
* Install AWS CLI
* Install GO 1.13+
* Install 3rd party library dependencies
  * Open a console and from the *app* folder run `go install`

## Running application
* Open a console and from the *app* folder run `go run app.go`
* Use Postman to send a request to `localhost:3000`

You will see processing output on your console

## Runnings tests
* Open a console and from the root folder run `go test -v ./... -cover`

## CI with AWS
* To test the lambda function locally run `sam local invoke -e event.json AuthFunction`
* Within Gitlab project, add your AWS keys under *Settings > CI/CD > variables*
  * AWS_DEFAULT_REGION
  * AWS_ACCESS_KEY_ID
  * AWS_SECRET_ACCESS_KEY
* To update the AWS function using AWS CLI manually run `aws lambda update-function-code --function-name cc-proc-go --zip-file fileb://build/cardsim.zip`

## References
* https://golang.org/
* https://gobyexample.com/
* https://tour.golang.org/list
* https://tutorialedge.net/golang/creating-restful-api-with-golang/
* https://medium.com/@kevalpatel2106/why-should-you-learn-go-f607681fad65
* https://www.golangprograms.com/goroutines.html
* https://medium.com/@masnun/making-http-requests-in-golang-dd123379efe7
* https://medium.com/dev-bits/making-concurrent-http-requests-in-go-programming-language-823b51bb1dc2
* https://docs.aws.amazon.com/cli/latest/reference/
* https://docs.gitlab.com/ee/user/project/clusters/serverless/aws.html

For setting up CI
* Reference to Gitlab CI: https://docs.gitlab.com/ee/ci/yaml/README.html
* Sample CI file: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Go.gitlab-ci.yml
* https://ronniegane.kiwi/blog/2019/06/18/go-gitlab/
